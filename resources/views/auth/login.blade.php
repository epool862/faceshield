@extends('layouts.volunteerauth')

@section('content')
<div class="account-pages"></div>
<div class="clearfix"></div>
<div class="wrapper-page">
    <div class="text-center">
        <a href="{{ url('/') }}" class="logo"><span>TeaMa <span>System</span></span></a>
    </div>
    <div class="m-t-20 card-box" style="padding: 10px !important">

        <div class="text-center">
            <h4 class="text-uppercase font-bold m-b-0">USER LOGIN</h4>
        </div>

        <div class="panel-body">
            <form method="POST" class="form-horizontal m-t-20" action="{{ route('login') }}">
                @csrf

                <div class="form-group @error('email') has-error @enderror">
                    <div class="col-xs-12">
                        <input type="text" class="form-control" id="email" name="email" placeholder="{{ __('E-Mail Address') }}" value="{{ old('email') }}">
                        @error('email')
                            <span class="text-danger" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>

                <div class="form-group @error('password') has-error @enderror">
                    <div class="col-xs-12">
                        <input type="password" class="form-control" id="password" name="password" placeholder="{{ __('Password') }}">
                        @error('password')
                            <span class="text-danger" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>

                <div class="form-group ">
                    <div class="col-xs-12">
                        <div class="checkbox checkbox-custom">
                            <input type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
                            <label for="remember">
                                {{ __('Remember Me') }}
                            </label>
                        </div>

                    </div>
                </div>

                <div class="form-group text-center m-t-30">
                    <div class="col-xs-12">
                        <button class="btn btn-danger btn-bordered btn-block waves-effect waves-light" type="submit">Log In</button>
                    </div>
                </div>

                <div class="form-group m-t-30 m-b-0">
                    <div class="col-sm-12 text-center">
                        <a href="{{ route('register') }}" class="text-muted"><i class="fa fa-user m-r-5"></i> Register New Account</a>
                    </div>
                </div>

            </form>

        </div>
    </div>
    <!-- end card-box-->

    <center>
        Web by <a href="https://azbahri.my">Azbahri</a>
    </center>
    
</div>
<!-- end wrapper page -->
@endsection
