@extends('layouts.volunteerauth')

@section('content')
<div class="account-pages"></div>
<div class="clearfix"></div>
<div class="wrapper-page" style="">
    <div class="text-center">
        <a href="{{ url('/') }}" class="logo"><span>TeaMa <span>System</span></span></a>
    </div>
    <div class="m-t-40 card-box" style="padding: 10px !important">

        <div class="text-center">
            <h4 class="text-uppercase font-bold m-b-0">USER REGISTRATION</h4>
        </div>

        <div class="panel-body">

            <form method="POST" class="m-t-20" action="{{ route('register') }}">
                @csrf

                <div class="form-group row @error('name') has-error @enderror">
                    <label for="name" class="col-md-4">Full Name</label>

                    <div class="col-md-8">
                        <input id="name" type="text" class="form-control input-sm" name="name" value="{{ old('name') }}">

                        @error('name')
                            <span class="text-danger" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>

                <div class="form-group row @error('email') has-error @enderror">
                    <label for="email" class="col-md-4">E-Mail Address</label>

                    <div class="col-md-8">
                        <input id="email" type="text" class="form-control input-sm" name="email" value="{{ old('email') }}">

                        @error('email')
                            <span class="text-danger" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>

                <div class="form-group row @error('password') has-error @enderror">
                    <label for="password" class="col-md-4">{{ __('Password') }}</label>

                    <div class="col-md-8">
                        <input id="password" type="password" class="form-control input-sm" name="password">

                        @error('password')
                            <span class="text-danger" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>

                <div class="form-group row @error('password_confirmation') has-error @enderror">
                    <label for="password-confirm" class="col-md-4">Confirm Passwd</label>

                    <div class="col-md-8">
                        <input id="password-confirm" type="password" class="form-control input-sm" name="password_confirmation">
                    </div>
                </div>

                <div class="form-group row text-center m-t-30">
                    <div class="col-xs-12">
                        <button class="btn btn-danger btn-bordered btn-block waves-effect waves-light" type="submit">Register</button>
                    </div>
                </div>

                <div class="form-group row m-t-30 m-b-0">
                    <div class="col-sm-12 text-center">
                        <a href="{{ route('login') }}" class="text-muted"><i class="fa fa-user m-r-5"></i> Already has account?</a>
                    </div>
                </div>

            </form>
                
        </div>
    </div>

    <center>
        Web by <a href="https://azbahri.my">Azbahri</a>
    </center>
    
</div>
@endsection