<!DOCTYPE html>
<!--[if IE 8 ]><html class="ie" xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-US" lang="en-US"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!-->
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-US" lang="en-US">
<!--<![endif]-->

<head>
    <!-- Basic Page Needs -->
    <meta charset="utf-8">
    <!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->
    <title>TeaMa Face Shield</title>

    <meta name="author" content="faceshield.site">

    <!-- Mobile Specific Metas -->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="keywords" content="3d printer, face shield, malaysia">
    <meta name="description" content="TeaMa Face Shield.">

    <meta property="og:title" content="TeaMa Face Shield" />
    <meta property="og:type" content="website" />
    <meta property="og:url" content="https://faceshield.site" />
    <meta property="og:image" content="https://faceshield.site/images/fb_thumb.png" />
    <meta property="og:site_name" content="TeaMa Face Shield" />
    <meta property="og:description" content="TeaMa Face Shield" />
    <meta property="fb:app_id" content="802949033218598" />

    <!-- Bootstrap  -->
    <link rel="stylesheet" type="text/css" href="{{ asset('stylesheets/bootstrap.css') }}">

    <!-- Theme Style -->
    <link rel="stylesheet" type="text/css" href="{{ asset('stylesheets/style.css') }}">

    <!-- Responsive -->
    <link rel="stylesheet" type="text/css" href="{{ asset('stylesheets/responsive.css') }}">

    <!-- Colors -->
    <link rel="stylesheet" type="text/css" href="{{ asset('stylesheets/colors/color1.css') }}" id="colors">

    <!-- Animation Style -->
    <link rel="stylesheet" type="text/css" href="{{ asset('stylesheets/animate.css') }}">

    <!-- Favicon and touch icons  -->
    <link href="{{ asset('icon/apple-touch-icon-48-precomposed.png') }}" rel="apple-touch-icon-precomposed"
        sizes="48x48">
    <link href="{{ asset('icon/apple-touch-icon-32-precomposed.png') }}" rel="apple-touch-icon-precomposed">
    <link href="{{ asset('icon/favicon.png') }}" rel="shortcut icon">

    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    @yield('head')
</head>

<body class="header-sticky page-loading header-v2">
    <div class="loading-overlay">
    </div>

    <!-- Boxed -->
    <div class="boxed">

        @include('includes.header')

        @yield('content')

        @include('includes.footer')

        <!-- Javascript -->
        <script type="text/javascript" src="javascript/jquery.min.js"></script>
        <script type="text/javascript" src="javascript/bootstrap.min.js"></script>
        <script type="text/javascript" src="javascript/jquery.easing.js"></script>
        <script type="text/javascript" src="javascript/jquery-waypoints.js"></script>
        <script type="text/javascript" src="javascript/jquery-countTo.js"></script>
        <script type="text/javascript" src="javascript/jquery.cookie.js"></script>
        <script type="text/javascript" src="javascript/parallax.js"></script>
        <script type="text/javascript" src="javascript/main.js"></script>

</body>

</html>