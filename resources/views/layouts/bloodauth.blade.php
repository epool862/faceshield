<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="Blood Donation">
        <meta name="author" content="Ahmad Saiful Bahri">

        <!-- App Favicon -->
        <link rel="shortcut icon" href="{{ asset('neqap/images/favicon.ico') }}">

        <!-- App title -->
        <title>Jom Derma Darah</title>

        <!-- App CSS -->
        <link href="{{ asset('neqap/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('neqap/css/core.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('neqap/css/components.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('neqap/css/icons.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('neqap/css/pages.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('neqap/css/menu.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('neqap/css/responsive.css') }}" rel="stylesheet" type="text/css" />

        <!-- HTML5 Shiv and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->

        <script src="{{ asset('neqap/js/modernizr.min.js') }}"></script>

        @yield('top_script')
        
    </head>
    <body>
        
        @yield('content')

        <script>
            var resizefunc = [];
        </script>

        <!-- jQuery  -->
        <script src="{{ asset('neqap/js/jquery.min.js') }}"></script>
        <script src="{{ asset('neqap/js/bootstrap.min.js') }}"></script>
        <script src="{{ asset('neqap/js/detect.js') }}"></script>
        <script src="{{ asset('neqap/js/fastclick.js') }}"></script>
        <script src="{{ asset('neqap/js/jquery.slimscroll.js') }}"></script>
        <script src="{{ asset('neqap/js/jquery.blockUI.js') }}"></script>
        <script src="{{ asset('neqap/js/waves.js') }}"></script>
        <script src="{{ asset('neqap/js/wow.min.js') }}"></script>
        <script src="{{ asset('neqap/js/jquery.nicescroll.js') }}"></script>
        <script src="{{ asset('neqap/js/jquery.scrollTo.min.js') }}"></script>

        <!-- App js -->
        <script src="{{ asset('neqap/js/jquery.core.js') }}"></script>
        <script src="{{ asset('neqap/js/jquery.app.js') }}"></script>

        @yield('bottom_script')
  
  </body>
</html>