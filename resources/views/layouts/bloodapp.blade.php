<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="Blood Donation">
        <meta name="author" content="Ahmad Saiful Bahri">

        <!-- App Favicon -->
        <link rel="shortcut icon" href="{{ asset('templates/images/favicon.ico') }}">

        <!-- App title -->
        <title>Jom Derma Darah</title>

        <!--Morris Chart CSS -->
		<link rel="stylesheet" href="{{ asset('neqap/plugins/morris/morris.css') }}">

        <!-- App css -->
        <link href="{{ asset('neqap/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('neqap/css/core.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('neqap/css/components.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('neqap/css/icons.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('neqap/css/pages.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('neqap/css/menu.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('neqap/css/responsive.css') }}" rel="stylesheet" type="text/css" />

        <!-- HTML5 Shiv and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->

        <script src="{{ asset('neqap/js/modernizr.min.js') }}"></script>

        @yield('top_script')

    </head>


    <body class="fixed-left">

        <!-- Begin page -->
        <div id="wrapper">

            <!-- Top Bar Start -->
            <div class="topbar">

                <!-- LOGO -->
                <div class="topbar-left">
                    <a href="" class="logo"><span>BLOOD <span>Donate</span></span><i class="zmdi zmdi-layers"></i></a>
                </div>

                <!-- Button mobile view to collapse sidebar menu -->
                <div class="navbar navbar-default" role="navigation">
                    <div class="container">

                        <!-- Page title -->
                        <ul class="nav navbar-nav navbar-left">
                            <li>
                                <button class="button-menu-mobile open-left">
                                    <i class="zmdi zmdi-menu"></i>
                                </button>
                            </li>
                            <li>
                                <h4 class="page-title">@yield('title')</h4>
                            </li>
                        </ul>

                        <!-- Right(Notification and Searchbox -->
                        <ul class="nav navbar-nav navbar-right">
                            <li>
                                <div class="m-t-20 hidden-xs" style="font-size: 16px;">
                                    Welcome <strong>{{ Auth::user()->name }}</strong>
                                </div>
                            </li>
                        </ul>

                    </div><!-- end container -->
                </div><!-- end navbar -->
            </div>
            <!-- Top Bar End -->


            <!-- ========== Left Sidebar Start ========== -->
            <div class="left side-menu">
                <div class="sidebar-inner slimscrollleft">

                    @include('includes.bloodmenu')

                </div>

            </div>
            <!-- Left Sidebar End -->


            <!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->
            <div class="content-page">
                <!-- Start content -->
                <div class="content">

                    <div class="container" style="min-height: 600px;">

                        @yield('content')

                    </div>

                </div>

                <footer class="footer text-right">
                    {{ date('Y') }} &copy; Blood Donation System
                </footer>

            </div>
            <!-- ============================================================== -->
            <!-- End Right content here -->
            <!-- ============================================================== -->


        </div>
        <!-- END wrapper -->

        <script>
            var resizefunc = [];
        </script>

        <!-- jQuery  -->
        <script src="{{ asset('neqap/js/jquery.min.js') }}"></script>
        <script src="{{ asset('neqap/js/bootstrap.min.js') }}"></script>
        <script src="{{ asset('neqap/js/detect.js') }}"></script>
        <script src="{{ asset('neqap/js/fastclick.js') }}"></script>
        <script src="{{ asset('neqap/js/jquery.blockUI.js') }}"></script>
        <script src="{{ asset('neqap/js/waves.js') }}"></script>
        <script src="{{ asset('neqap/js/jquery.nicescroll.js') }}"></script>
        <script src="{{ asset('neqap/js/jquery.slimscroll.js') }}"></script>
        <script src="{{ asset('neqap/js/jquery.scrollTo.min.js') }}"></script>

        <!-- KNOB JS -->
        <!--[if IE]>
        <script type="text/javascript" src="{{ asset('templates/plugins/jquery-knob/excanvas.js') }}"></script>
        <![endif]-->
        <script src="{{ asset('neqap/plugins/jquery-knob/jquery.knob.js') }}"></script>

        <!--Morris Chart-->
        <script src="{{ asset('neqap/plugins/morris/morris.min.js') }}"></script>
        <script src="{{ asset('neqap/plugins/raphael/raphael-min.js') }}"></script>

        <!-- App js -->
        <script src="{{ asset('neqap/js/jquery.core.js') }}"></script>
        <script src="{{ asset('neqap/js/jquery.app.js') }}"></script>

        @yield('bottom_script')

    </body>
</html>