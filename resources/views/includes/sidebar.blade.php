<?php 
$path_url = ""; 
$class_active = "active";
$class_default = "";

$currentFullUrl = url()->current();
$lastUrltmp = explode("/", $currentFullUrl);
$lastUrl = end($lastUrltmp);
?>
<div class="general-sidebars">
    <div class="sidebar-wrap">
        <div class="sidebar">
            <div class="widget widget_nav_menu">
                <ul class="nav_menu">
                    <li class="menu-item">
                        <a class="@if(starts_with($currentFullUrl, route('request'))) active @endif" href="{{ route('request') }}">Request Face Shield</a>
                    </li>
                    <li class="menu-item">
                        <a class="@if(starts_with($currentFullUrl, route('volunteer'))) active @endif" href="{{ route('volunteer') }}">Become a Volunteer</a>
                    </li>
                    <li class="menu-item">
                        <a class="@if(starts_with($currentFullUrl, route('donate'))) active @endif" href="{{ route('donate') }}">Make a Donation</a>
                    </li>
                    <li class="menu-item">
                        <a class="@if(starts_with($currentFullUrl, route('faq'))) active @endif" href="{{ route('faq') }}">FAQ</a>
                    </li>
                </ul>
            </div><!-- /.widget_nav_menu -->

            @if(1==2)
            <div class="widget widget_recent_entries">
                <h4 class="widget-title">Recent update</h4>
                <ul>
                    <li>
                        <a href="blog-single.html">Raising productivity &amp; morale in the warehouse</a>
                        <span class="post-date">March 25, 2016</span>
                    </li>
                    <li>
                        <a href="blog-single.html">Seafield logistics goes into administration</a>
                        <span class="post-date">March 25, 2016</span>
                    </li>
                </ul>
            </div><!-- /.widget_recent_entries -->
            @endif

        </div><!-- /.sidebar -->
    </div><!-- /.sidebar-wrap -->
</div><!-- /.general-sidebars -->