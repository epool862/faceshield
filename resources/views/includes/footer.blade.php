@php
$data = App\Data::latest()->first();
@endphp
<!-- Footer -->
<footer class="footer">
    <div class="content-bottom-widgets">
        <div class="container">
            <div class="row">
                <div class="flat-wrapper">
                    <div class="ft-wrapper clearfix">
                        <div class="footer-50">
                            <div class="row">
                                <div class="col-md-5">
                                    <div class="counter">
                                        <div class="counter-image"><i class="fa fa-money"></i></div>
                                        <div class="numb-count" data-to="7000" data-speed="3000"
                                            data-waypoint-active="yes">7000</div>
                                        <div class="counter-title">
                                            Remaining (MYR)
                                        </div>
                                    </div>
                                </div><!-- /.col-md-4 -->

                                <div class="col-md-3">
                                    <div class="counter ft-style1">
                                        <div class="counter-image"><i class="fa fa-print"></i></div>
                                        <div class="numb-count" data-to="301" data-speed="3000"
                                            data-waypoint-active="yes">301</div>
                                        <div class="counter-title">
                                            Volunteer's Printer
                                        </div>
                                    </div>
                                </div><!-- /.col-md-4 -->

                                @if(1==2)
                                <div class="col-md-4">
                                    <div class="counter ft-style2">
                                        <div class="counter-image"><i class="fa fa-truck"></i></div>
                                        <div class="numb-count" data-to="{{ $data->delivered }}" data-speed="3000"
                                            data-waypoint-active="yes">{{ $data->delivered }}</div>
                                        <div class="counter-title">
                                            Faceshield Delivered
                                        </div>
                                    </div>
                                </div><!-- /.col-md-4 -->
                                @endif

                            </div><!-- /.row -->
                        </div><!-- /.footer-50 -->

                        <div class="footer-50">
                            <div class="subscribe-form">
                                <div class="row">
                                    <div class="col-md-8">
                                        <input type="email" name="EMAIL" placeholder="Your email address" required="">
                                    </div><!-- /.col-md-8 -->

                                    <div class="col-md-4">
                                        <input type="submit" value="Subscribe">
                                    </div><!-- /.col-md-4 -->
                                </div><!-- /.row -->
                            </div><!-- /.subscribe-form -->
                        </div><!-- /.footer-50 -->
                    </div><!-- /.ft-wrapper -->
                </div><!-- /.flat-wrapper -->
            </div><!-- /.row -->
        </div><!-- /.container -->
    </div><!-- /.footer-widgets -->

    <div class="footer-widgets">
        <div class="container">
            <div class="row">
                <div class="col-md-3">
                    <div class="widge widget_text">
                        <div class="textwidget">
                            <h2>TeaMa Face Shield</h2>
                        </div>
                    </div><!-- /.widget_text -->
                </div><!-- /.col-md-3 -->

                <div class="col-md-3">
                    <div class="widget widget_recent_entries">
                        <h4 class="widget-title">News</h4>
                        <ul>
                            <li>
                                <a target="_blank"
                                    href="https://www.thestar.com.my/tech/tech-news/2020/03/23/malaysian-3d-printers-produce-face-shields-to-aid-frontliners-in-the-fight-against-covid-19">Malaysian
                                    3D printing enthusiasts produce face shields to aid frontliners in fight against
                                    pandemic</a>
                                <span class="post-date">The Star</span>
                            </li>
                            <li>
                                <a target="_blank"
                                    href="https://www.malaymail.com/news/malaysia/2020/03/23/with-face-shields-in-short-supply-malaysians-bring-3d-printers-into-covid-1/1849377">With
                                    face shields in short supply, Malaysians bring 3D printers into Covid-19 fight</a>
                                <span class="post-date">Malaymail</span>
                            </li>
                        </ul>
                    </div><!-- /.widget_recent_entries -->
                </div><!-- /.col-md-3 -->

                <div class="col-md-3">
                    <div class="widget">
                        <h3 class="widget-title">Useful Links</h3>
                        <div class="">
                            <ul class="">
                                <li>- <a target="_blank"
                                        href="http://www.moh.gov.my/index.php/pages/view/2019-ncov-wuhan">Ministry of
                                        Health</a></li>
                                <li>- <a target="_blank" href="https://www.doctoroncall.com.my/coronavirus">Doctor On
                                        Call</a></li>
                                <li>- <a target="_blank" href="https://www.mkn.gov.my/">Majlis Keselamatan Negara</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div><!-- /.col-md-3 -->

                <div class="col-md-3">
                    <div class="widget widget_text information">
                        <h3 class="widget-title">Contact</h3>
                        <div class="textwidget">
                            <p style="color: #FFF;"><strong>Logistic & Communication</strong></p>
                            <p>
                                <a href="mailto:adrian@cornerroomstudios.com" target="_blank"><i
                                        class="fa fa-envelope"></i>adrian@cornerroomstudios.com</a> <br>(Adrian Wong)
                            </p>
                        </div>
                    </div>
                </div><!-- /.col-md-3 -->
            </div><!-- /.row -->
        </div><!-- /.container -->
    </div><!-- /.footer-content -->

    <div class="footer-content">
        <div class="container">
            <div class="row">
                <div class="flat-wrapper">
                    <div class="ft-wrap clearfix">
                        <div class="social-links">
                            <a href="https://www.facebook.com/groups/teamacovid19" target="_blank"><i
                                    class="fa fa-facebook-official"></i></a>
                        </div>
                        <div class="copyright">
                            <div class="copyright-content">
                                Web by <a href="https://azbahri.my">Azbahri</a>
                            </div>
                        </div>
                    </div><!-- /.ft-wrap -->
                </div><!-- /.flat-wrapper -->
            </div><!-- /.row -->
        </div><!-- /.container -->
    </div><!-- /.footer-content -->
</footer>

<!-- Go Top -->
<a class="go-top">
    <i class="fa fa-chevron-up"></i>
</a>

</div>