<?php 
$path_url = ""; 
$class_active = "home";
$class_default = "";

$currentFullUrl = url()->current();
$lastUrltmp = explode("/", $currentFullUrl);
$lastUrl = end($lastUrltmp);
?>
<div class="site-header header-v2">
    <div class="flat-top">
        <div class="container hidden-xs">
            <div class="row">
                <div class="flat-wrapper">
                    <div class="custom-info hidden-xs">
                        <span>TeaMa Face Shield</span>
                    </div>

                    <div class="social-links">
                        <a href="https://www.facebook.com/groups/teamacovid19" target="_blank">
                            <i class="fa fa-facebook-official"></i> &nbsp;Join our Facebook Group
                        </a>
                    </div>
                </div><!-- /.flat-wrapper -->
            </div><!-- /.row -->
        </div><!-- /.container -->
    </div><!-- /.flat-top -->

    <header id="header" class="header">
        <div class="header-wrap">
            <div id="logo" class="logo">
                <a href="{{ route('home') }}">
                    <img src="images/logo.png" alt="images">
                </a>
            </div><!-- /.logo -->
            <div class="btn-menu">
                <span></span>
            </div><!-- //mobile menu button -->

            <div class="nav-wrap">
                <nav id="mainnav" class="mainnav">

                    <ul class="menu">
                        <li class="@if($currentFullUrl == route('home')) {{ $class_active }} @endif">
                            <a href="{{ route('home') }}">Home</a>
                        </li>
                        <li class="@if(starts_with($currentFullUrl, route('request'))) {{ $class_active }} @endif">
                            <a href="{{ route('request') }}">Order/Request</a>
                        </li>
                        <li class="@if(starts_with($currentFullUrl, route('product'))) {{ $class_active }} @endif">
                            <a href="#">#BreakTheChain</a>
                            <ul class="submenu">
                                <li><a href="{{ route('product') . '?id=1' }}">Face Shield</a></li>
                                <li><a href="{{ route('product') . '?id=2'}}">Intubation Box</a></li>
                                <li><a href="{{ route('product') . '?id=3'}}">Dmask</a></li>
                            </ul><!-- /.submenu -->
                        </li>
                        <li class="@if(starts_with($currentFullUrl, route('map'))) {{ $class_active }} @endif">
                            <a href="{{ route('map') }}">Location</a>
                        </li>
                        <li class="@if(starts_with($currentFullUrl, route('volunteer'))) {{ $class_active }} @endif">
                            <a href="{{ route('volunteer') }}">Volunteer</a>
                        </li>
                        <li class="@if(starts_with($currentFullUrl, route('faq'))) {{ $class_active }} @endif">
                            <a href="{{ route('faq') }}">F.A.Q. &nbsp; &nbsp; &nbsp; &nbsp; </a>
                        </li>
                        <li class="hidden-lg hidden-md"><a href="{{ route('donate') }}">Make a donation</a></li>
                        <a class="btn btn-danger hidden-xs" href="{{ route('donate') }}">Make a donation</a>
                    </ul><!-- /.menu -->
                </nav><!-- /.mainnav -->
            </div><!-- /.nav-wrap -->
        </div><!-- /.header-wrap -->
    </header><!-- /.header -->
</div><!-- /.site-header -->

<!-- Page title -->
<div class="page-title parallax-style parallax1">
    <div class="overlay"></div>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="page-title-heading">
                    <h2>TeaMa Face Shield</h2>
                </div><!-- /.page-title-heading -->
            </div><!-- /.col-md-12 -->
        </div><!-- /.row -->
    </div><!-- /.container -->
</div><!-- /.page-title -->