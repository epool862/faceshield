<!--- Sidemenu -->
<div id="sidebar-menu">
    <ul>

        <li>
            <a href="{{ route('admin.dashboard') }}" class="waves-effect"><i class="zmdi zmdi-view-dashboard"></i> <span> Dashboard </span> </a>
        </li>

        <li class="has_sub">
            <a href="javascript:void(0);" class="waves-effect"><i class="zmdi zmdi-assignment"></i> <span> Results </span> <span class="menu-arrow"></span></a>
            <ul class="list-unstyled">
                <li><a href="{{ route('admin.list') }}">List All</a></li>
                <li><a href="{{ route('admin.export') }}">Export Data</a></li>
            </ul>
        </li>

        <li>
            <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();" class="waves-effect"><i class="zmdi zmdi-power"></i><span> Logout </span></a>
        </li>

        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
            @csrf
        </form>

    </ul>
    <div class="clearfix"></div>
    <hr>
    <ul>
        <li>
            <div style="padding-left: 30px;">
                <p>
                    <span class="text-danger"><strong>TeaMa System</strong></span><br>
                    <span class="text-muted">Version 1.0 (BETA)</span>
                </p>
            </div>
        </li>
    </ul>
    <ul>
        <li>
            <div style="padding-left: 30px;">
                <p>
                    <span>Developed by <a href="https://azbahri.my" target="_blank">Azbahri</a></span>
                </p>
            </div>
        </li>
    </ul>
</div>
<!-- Sidebar -->
<div class="clearfix"></div>