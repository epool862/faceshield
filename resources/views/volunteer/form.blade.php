@extends('layouts.volunteerauth')

@section('top_script')
<style type="text/css">
  #map{ width:auto; height: 300px; margin-bottom: 10px; margin-right: -25px; margin-left: -25px; }
</style>
@endsection

@section('content')
<div class="account-pages"></div>
<div class="clearfix"></div>
<div class="wrapper-page" style="">
    <div class="text-center">
        <a href="{{ url('/') }}" class="logo"><span><span>Volunteer's</span> Location</span></a>
    </div>
    <div class="m-t-20 card-box" style="padding: 10px !important">

        <div class="text-center">
            <h4 class="text-uppercase font-bold m-b-0">VOLUNTEER INFORMATION</h4>
        </div>

        <div class="panel-body">

            <form method="POST" class="m-t-20" action="{{ route('volunteerpost') }}">
                @csrf

                <div class="form-group row @error('name') has-error @enderror">
                    <label for="name" class="col-md-4">Volunteer Name</label>

                    <div class="col-md-8">
                        <input id="name" type="text" class="form-control input-sm" name="name" value="{{ old('name') }}">

                        @error('name')
                            <span class="text-danger" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>

                <div class="form-group row @error('mobile') has-error @enderror">
                    <label for="mobile" class="col-md-4">Contact No</label>

                    <div class="col-md-8">
                        <input id="mobile" type="text" class="form-control input-sm" name="mobile" value="{{ old('mobile') }}">

                        @error('mobile')
                            <span class="text-danger" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>

                <div class="form-group row @error('printer_model') has-error @enderror">
                    <label for="printer_model" class="col-md-4">Printer Model</label>

                    <div class="col-md-8">
                        <input id="printer_model" type="text" class="form-control input-sm" name="printer_model" value="{{ old('printer_model') }}">

                        @error('printer_model')
                            <span class="text-danger" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>

                <div class="form-group row @error('printer_all') has-error @enderror">
                    <label for="printer_all" class="col-md-4">No of Printer</label>

                    <div class="col-md-8">
                        <input id="printer_all" type="text" class="form-control input-sm" name="printer_all" value="{{ old('printer_all') }}" style="max-width: 100px;">

                        @error('printer_all')
                            <span class="text-danger" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>

                <div class="form-group row @error('perday') has-error @enderror">
                    <label for="perday" class="col-md-4">Production/Day</label>

                    <div class="col-md-8">
                        <input id="perday" type="text" class="form-control input-sm" name="perday" value="{{ old('perday') }}" style="max-width: 100px;">

                        @error('perday')
                            <span class="text-danger" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>

                <div class="form-group row @error('address_1') has-error @enderror">
                    <label for="address_1" class="col-md-4">Address</label>

                    <div class="col-md-8">
                        <input id="address_1" type="text" class="form-control input-sm" name="address_1" value="{{ old('address_1') }}">

                        @error('address_1')
                            <span class="text-danger" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>

                <div class="form-group row @error('postcode') has-error @enderror">
                    <label for="postcode" class="col-md-4">Postcode</label>

                    <div class="col-md-8">
                        <input id="postcode" type="text" class="form-control input-sm" name="postcode" value="{{ old('postcode') }}">

                        @error('postcode')
                            <span class="text-danger" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>

                <div class="form-group row @error('city') has-error @enderror">
                    <label for="city" class="col-md-4">City/District</label>

                    <div class="col-md-8">
                        <input id="city" type="text" class="form-control input-sm" name="city" value="{{ old('city') }}">

                        @error('city')
                            <span class="text-danger" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>

                <div class="form-group row @error('state') has-error @enderror">
                    <label for="state" class="col-md-4">State</label>

                    <div class="col-md-8">
                        <select class="form-control input-sm" style="" name="state" id="state" onchange="resetMap();">
                            <option @if(old('state') == "Johor") selected="selected" @endif value="Johor">Johor</option>
                            <option @if(old('state') == "Kedah") selected="selected" @endif value="Kedah">Kedah</option>
                            <option @if(old('state') == "Kelantan") selected="selected" @endif value="Kelantan">Kelantan</option>
                            <option @if(old('state') == "Melaka") selected="selected" @endif value="Melaka">Melaka</option>
                            <option @if(old('state') == "Negeri Sembilan") selected="selected" @endif value="Negeri Sembilan">Negeri Sembilan</option>
                            <option @if(old('state') == "Pahang") selected="selected" @endif value="Pahang">Pahang</option>
                            <option @if(old('state') == "Perak") selected="selected" @endif value="Perak">Perak</option>
                            <option @if(old('state') == "Perlis") selected="selected" @endif value="Perlis">Perlis</option>
                            <option @if(old('state') == "Pulau Pinang") selected="selected" @endif value="Pulau Pinang">Pulau Pinang</option>
                            <option @if(old('state') == "Sabah") selected="selected" @endif value="Sabah">Sabah</option>
                            <option @if(old('state') == "Sarawak") selected="selected" @endif value="Sarawak">Sarawak</option>
                            <option @if(old('state') == "Selangor") selected="selected" @endif value="Selangor">Selangor</option>
                            <option @if(old('state') == "Terengganu") selected="selected" @endif value="Terengganu">Terengganu</option>
                            <option @if(old('state') == "WP Kuala Lumpur") selected="selected" @endif value="WP Kuala Lumpur">WP Kuala Lumpur</option>
                            <option @if(old('state') == "WP Labuan") selected="selected" @endif value="WP Labuan">WP Labuan</option>
                            <option @if(old('state') == "WP Putrajaya") selected="selected" @endif value="WP Putrajaya">WP Putrajaya</option>
                        </select>

                        @error('state')
                            <span class="text-danger" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>

                <br>

                <div class="text-center">
                    <h3 class="text-uppercase font-bold m-b-0">PLEASE PIN LOCATION</h3>
                    <br>
                    <div>
                    @error('gmap_lat')
                        <span class="text-danger" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                    </div>

                    <div>
                    @error('gmap_long')
                        <span class="text-danger" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                    </div>
                </div>

                <div id="map"></div>

                <div class="row">
                    <div class="col-xs-6">
                        <table>
                            <tr>
                                <td width="40" valign="middle">Lat: &nbsp; </td>
                                <td>
                                    <input type="text" style="border:none; background-color: #EEE; width: 100%; padding: 5px; font-weight: bold;" id="gmap_lat" name="gmap_lat" value="" readonly="yes">
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div class="col-xs-6">
                        <table>
                            <tr>
                                <td width="45" valign="middle">Long: &nbsp; </td>
                                <td>
                                    <input type="text" style="border:none; background-color: #EEE; width: 100%; padding: 5px; font-weight: bold;" id="gmap_long" name="gmap_long" value="" readonly="yes">
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>

                <br>

                <div class="form-group row text-center m-t-30">
                    <div class="col-xs-12">
                        <button class="btn btn-danger btn-bordered btn-block waves-effect waves-light" type="submit">Submit</button>
                    </div>
                </div>

                <div class="form-group row m-t-30 m-b-0">
                    <div class="col-sm-12 text-center">
                        
                    </div>
                </div>

            </form>
                
        </div>
    </div>

    <center>
        Web by <a href="https://azbahri.my">Azbahri</a>
    </center>
</div>
@endsection

@section('bottom_script')
<script type="text/javascript">
    //Set up some of our variables.
    var map; //Will contain map object.
    var marker = false; ////Has the user plotted their location marker? 
    var infoWindow;
    var es = document.getElementById("state");
    var esv = es.options[es.selectedIndex].value;
    var allowed = 0;

    var state = [];

    state['Selangor'] = [3.066924319962923, 101.53106692247096];
    state['Pahang'] = [3.7984839750369748, 103.1982419639826];
    state['Johor'] = [1.5891512317919343, 103.63037064671518];
    state['Perak'] = [4.554978434796619, 101.0998535435647];
    state['Perlis'] = [6.394915021052554, 100.17974864691497];
    state['Pulau Pinang'] = [5.400463656565281, 100.31158450059596];
    state['Kelantan'] = [6.090032283665036, 102.24609380587938];
    state['Terengganu'] = [5.3330116683433735, 103.09753423556688];
    state['Negeri Sembilan'] = [2.697062518434213, 101.92840576171876];
    state['Kedah'] = [5.644685902869445, 100.5404663644731];
    state['Sarawak'] = [1.5397315200514765, 110.3567505441606];
    state['Sabah'] = [5.945720779973998, 116.08795168809593];
    state['Melaka'] = [2.196498521131974, 102.25044250488283];
    state['WP Kuala Lumpur'] = [3.147372266200593, 101.68395996093751];
    state['WP Putrajaya'] = [2.899609932002293, 101.66839602403343];
    state['WP Labuan'] = [5.284240872135917, 115.24017331190409];
            
    //Function called to initialize / create the map.
    //This is called when the page has loaded.
    function initMap() {
     
        //The center location of our map.
        var centerOfMap = new google.maps.LatLng(state[esv][0], state[esv][1]);
        infoWindow = new google.maps.InfoWindow;
     
        //Map options.
        var options = {
          center: centerOfMap, //Set center.
          zoom: 10, //The zoom value.
          streetViewControl: false,
          mapTypeControl: false,
          gestureHandling: 'greedy',
          //disableDefaultUI: true, 
        };
     
        //Create the map object.
        map = new google.maps.Map(document.getElementById('map'), options);

        // Try HTML5 geolocation.
        if (navigator.geolocation) {
          navigator.geolocation.getCurrentPosition(function(position) {
            var pos = {
              lat: position.coords.latitude,
              lng: position.coords.longitude
            };

            //infoWindow.setPosition(pos);
            //infoWindow.setContent('Location found.');
            //infoWindow.open(map);
            map.setZoom(14);
            map.setCenter(pos);
            allowed = 1;

            //If the marker hasn't been added.
            if(marker === false){
                //Create the marker.
                marker = new google.maps.Marker({
                    position: pos,
                    map: map,
                    draggable: true //make it draggable
                });
                //Listen for drag events!
                google.maps.event.addListener(marker, 'dragend', function(event){
                    markerLocation();
                });
                map.setZoom(15);
                map.setCenter(pos);
            } else{
                //Marker has already been added, so just change its location.
                marker.setPosition(pos);
                map.setZoom(15);
                map.setCenter(pos);
            }
            //Get the marker's location.
            markerLocation();

          }, function() {
            handleLocationError(true, infoWindow, map.getCenter());
          });
        } else {

            var pos = {
              lat: state[esv][0],
              lng: state[esv][1],
            };
            map.setZoom(10);
            map.setCenter(pos);
          // Browser doesn't support Geolocation
          handleLocationError(false, infoWindow, map.getCenter());
        }
     
        //Listen for any clicks on the map.
        google.maps.event.addListener(map, 'click', function(event) {                
            //Get the location that the user clicked.
            var clickedLocation = event.latLng;
            //If the marker hasn't been added.
            if(marker === false){
                //Create the marker.
                marker = new google.maps.Marker({
                    position: clickedLocation,
                    map: map,
                    draggable: true //make it draggable
                });
                //Listen for drag events!
                google.maps.event.addListener(marker, 'dragend', function(event){
                    markerLocation();
                });
                map.setZoom(15);
                map.setCenter(clickedLocation);
            } else{
                //Marker has already been added, so just change its location.
                marker.setPosition(clickedLocation);
                map.setZoom(15);
                //map.setCenter(clickedLocation);
            }
            //Get the marker's location.
            markerLocation();
        });
    }
            
    //This function will get the marker's current location and then add the lat/long
    //values to our textfields so that we can save the location.
    function markerLocation(){
        //Get location.
        var currentLocation = marker.getPosition();
        //Add lat and lng values to a field that we can save.
        document.getElementById('gmap_lat').value = currentLocation.lat(); //latitude
        document.getElementById('gmap_long').value = currentLocation.lng(); //longitude
    }

    function handleLocationError(browserHasGeolocation, infoWindow, pos) {
        //infoWindow.setPosition(pos);
        //infoWindow.setContent(browserHasGeolocation ?
                             // 'Error: The Geolocation service failed.' :
                              //'Error: Your browser doesn\'t support geolocation.');
        //infoWindow.open(map);
    }

    function resetMap(){

        if(allowed == 0){
            var esv = es.options[es.selectedIndex].value;

            var pos = {
              lat: state[esv][0],
              lng: state[esv][1],
            };
            map.setZoom(10);
            map.setCenter(pos);
        }

    }
            
            
    //Load the map when the page has finished loading.
    
</script>
<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAKYX9L4rJuADsFoufmW7MQBUyyfo9j18M&callback=initMap"></script>
@endsection