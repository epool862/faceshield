@extends('layouts.volunteerapp')

@section('title') Results @endsection

@section('top_script')
<!-- DataTables -->
<link href="{{ asset('neqap/plugins/datatables/jquery.dataTables.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('neqap/plugins/datatables/buttons.bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('neqap/plugins/datatables/fixedHeader.bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('neqap/plugins/datatables/responsive.bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('neqap/plugins/datatables/scroller.bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('content')
<div class="row m-t-20">

    <div class="col-lg-12">
        <div class="card-box">
            <div class="dropdown pull-right">
                <a href="#" class="dropdown-toggle card-drop" data-toggle="dropdown" aria-expanded="false">
                    <i class="zmdi zmdi-more-vert"></i>
                </a>
                <ul class="dropdown-menu" role="menu">
                    <li><a href="#">Unavailable</a></li>
                </ul>
            </div>

            <h4 class="header-title m-t-0 m-b-30">All Records</h4>

            <div class="">
                <table id="datatable" class="table table-striped table-bordered">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Name</th>
                        <th>Mobile</th>
                        <th>Location</th>
                        <th>Status</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                        @php($i = 1)
                        @foreach($volunteers as $volunteer)
                        <tr>
                            <td>{{ $i++ }}</td>
                            <td>{{ $volunteer->name }}</td>
                            <td>{{ $volunteer->mobile }}</td>
                            <td>
                                {{ $volunteer->postcode }} {{ $volunteer->city }}, {{ $volunteer->state }}<br>
                                <span><small>{{ $volunteer->gmap_lat }}, {{ $volunteer->gmap_long }}</small></span>
                            </td>
                            <td><span class="label label-danger">New</span></td>
                            <td>
                                
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div><!-- end col -->

</div>
<!-- end row -->
@endsection

@section('bottom_script')
<!-- Datatables-->
<script src="{{ asset('neqap/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('neqap/plugins/datatables/dataTables.bootstrap.js') }}"></script>
<script src="{{ asset('neqap/plugins/datatables/dataTables.buttons.min.js') }}"></script>
<script src="{{ asset('neqap/plugins/datatables/buttons.bootstrap.min.js') }}"></script>
<script src="{{ asset('neqap/plugins/datatables/jszip.min.js') }}"></script>
<script src="{{ asset('neqap/plugins/datatables/pdfmake.min.js') }}"></script>
<script src="{{ asset('neqap/plugins/datatables/vfs_fonts.js') }}"></script>
<script src="{{ asset('neqap/plugins/datatables/buttons.html5.min.js') }}"></script>
<script src="{{ asset('neqap/plugins/datatables/buttons.print.min.js') }}"></script>
<script src="{{ asset('neqap/plugins/datatables/dataTables.fixedHeader.min.js') }}"></script>
<script src="{{ asset('neqap/plugins/datatables/dataTables.keyTable.min.js') }}"></script>
<script src="{{ asset('neqap/plugins/datatables/dataTables.responsive.min.js') }}"></script>
<script src="{{ asset('neqap/plugins/datatables/responsive.bootstrap.min.js') }}"></script>
<script src="{{ asset('neqap/plugins/datatables/dataTables.scroller.min.js') }}"></script>

<script type="text/javascript">
    $(document).ready(function() {
        $('#datatable').dataTable();
    });
</script>
@endsection