@extends('layouts.volunteerauth')

@section('top_script')
<style type="text/css">
  #map{ width:auto; height: 300px; margin-bottom: 10px; margin-right: -25px; margin-left: -25px; }
</style>
@endsection

@section('content')
<div class="account-pages"></div>
<div class="clearfix"></div>
<div class="wrapper-page" style="">
    <div class="text-center">
        <a href="{{ url('/') }}" class="logo"><span><span>Volunteer's</span> Location</span></a>
    </div>
    <div class="m-t-20 card-box" style="padding: 10px !important">

        <div class="text-center">
            <h3 class="text-uppercase font-bold m-b-0 m-t-20" style="color: green;">THANK YOU</h3>
        </div>

        <div class="panel-body">

            <div class="row m-t-30 m-b-0">
                <div class="col-sm-12 text-center">
                    <p><a href="{{ route('home') }}" class="btn btn-danger">Return Home</a></p>
                </div>
            </div>
                
        </div>
    </div>

    <center>
        Web by <a href="https://azbahri.my">Azbahri</a>
    </center>
    
</div>
@endsection

@section('bottom_script')
<script type="text/javascript">
</script>
@endsection