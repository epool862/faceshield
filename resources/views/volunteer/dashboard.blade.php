@extends('layouts.volunteerapp')

@section('title') Dashboard @endsection

@section('top_script')

@endsection

@section('content')
<div class="row m-t-20">

    <div class="col-lg-12">
        <div class="card-box">
            <div class="dropdown pull-right">
                <a href="#" class="dropdown-toggle card-drop" data-toggle="dropdown" aria-expanded="false">
                    <i class="zmdi zmdi-more-vert"></i>
                </a>
                <ul class="dropdown-menu" role="menu">
                    <li><a href="#">Unavailable</a></li>
                </ul>
            </div>

            <h4 class="header-title m-t-0 m-b-30">Welcome</h4>
            <hr>
            <h5>{{ Auth::user()->name }}</h5>

        </div>
    </div><!-- end col -->

</div>
<!-- end row -->
@endsection

@section('bottom_script')

@endsection