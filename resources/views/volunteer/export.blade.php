@extends('layouts.volunteerapp')

@section('title') Export @endsection

@section('top_script')

@endsection

@section('content')
<div class="row m-t-20">

    <div class="col-lg-12">
        <div class="card-box">
            <div class="dropdown pull-right">
                <a href="#" class="dropdown-toggle card-drop" data-toggle="dropdown" aria-expanded="false">
                    <i class="zmdi zmdi-more-vert"></i>
                </a>
                <ul class="dropdown-menu" role="menu">
                    <li><a href="#">Unavailable</a></li>
                </ul>
            </div>

            <h4 class="header-title m-t-0 m-b-30">Export Excel</h4>
            <hr>
            <div style="margin-bottom: 5px;">Click button below to download the excel file.</div>
            <a href="#" class="btn btn-danger"><i class="fa fa-save"></i> &nbsp; This feature coming soon</a>

        </div>
    </div><!-- end col -->

</div>
<!-- end row -->
@endsection

@section('bottom_script')

@endsection