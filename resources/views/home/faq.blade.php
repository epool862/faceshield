@extends('layouts.index')

@section('content')
<div class="page-breadcrumbs">
    <div class="container">
        <div class="row">
            <div class="flat-wrapper">
                <div class="breadcrumbs">
                    <h2 class="trail-browse">You are here:</h2>
                    <ul class="trail-items">
                        <li class="trail-item"><a href="{{ route('home') }}">Homepage</a></li>
                        <li>Read our FAQ</li>
                    </ul>
                </div><!-- /.breadcrumbs -->
            </div><!-- /.flat-wrapper -->
        </div><!-- /.row -->
    </div><!-- /.container -->
</div><!-- /.page-breadcrumbs -->

<div class="flat-row flat-general sidebar-left pad-bottom80px">
    <div class="container">
        <div class="row">
            <div class="general">

                <h3 class="flat-title-section style">Read our <span>FAQ</span></h3>

                <div class="flat-divider d30px"></div>              

                <div class="flat-accordion style">
                    <div class="flat-toggle">
                        <h6 class="toggle-title active">1. Are you a for-profit organization?</h6>
                        <div class="toggle-content">
                            <p>Nopes! We are a group of around 35 3D printer enthusiasts (some are businesses) that
                            have banded together to help out! The gap that we see at hospitals and clinics is that
                            they are running out/low on PPE, and some doctors and nurses are DIY-ing PPEs
                            themselves. We feel that this is where we can step in to help : we print the face shields
                            while they do what they do best which is taking care of patients! </p>
                        </div><!-- /.toggle-content -->
                    </div><!-- /.flat-toggle -->

                    <div class="flat-toggle">
                        <h6 class="toggle-title">2. What type of PPE are you making?</h6>
                        <div class="toggle-content">
                            <p>We are making face shields. The frames are made by 3D printers. The plastic shield
                            (covering the face) can be easily assembled by the end user.</p><br>
                            <img src="{{ asset('images/faceshield.jpg') }}">
                        </div><!-- /.toggle-content -->
                    </div><!-- /.flat-toggle -->

                    <div class="flat-toggle">
                        <h6 class="toggle-title">3. Are your face shields approved by the Ministry of Health or Kementerian Kesihatan
                        Malaysia?</h6>
                        <div class="toggle-content">
                            <p>Not at the moment. However, we take comfort in the fact that samples of our face
                            shields have been sent to various doctors and nurses at the frontlines, and that the
                            feedback has been positive!</p>
                            <p>For now, our face shields are deemed good enough to be used at <span style="color: red; font-weight: bold;">low risk areas or
                            operations.</span> These areas include screening areas at hospitals/clinics, normal clinics (e.g.
                            klinik kesihatan daerah), quarantine stations, or as extra protection at normal wards
                            which are non-COVID related. <strong>This will ensure that the high-grade PPEs that are coming
                            in from the government can be fully utilized at high risk areas such as wards that house
                            COVID-19 positive patients.</strong></p>
                            <p>Hence, we are complementing the work of the government and filling a need as
                            highlighted by medical practitioners nationwide. </p>
                        </div><!-- /.toggle-content -->
                    </div><!-- /.flat-toggle -->

                    <div class="flat-toggle">
                        <h6 class="toggle-title">4. So, what are you raising funds for?</h6>
                        <div class="toggle-content">
                            <p>As mentioned, we are a NON-PROFIT ORGANIZATION. The funds raised will be used to
                            buy materials for production and to help subsidize the cost of delivery of the face shields. </p>
                            <p>You can make your donation to this account : <br>
                            <strong>Maybank account name : Corner Room Studios Sdn Bhd<br>
                            Maybank account number: 562209619966</strong></p>
                        </div><!-- /.toggle-content -->
                    </div><!-- /.flat-toggle -->

                    <div class="flat-toggle">
                        <h6 class="toggle-title">5. Are your face shields sanitized before use?</h6>
                        <div class="toggle-content">
                            <p>We ensure that our places of production are clean and hygienic. However, the areas are not STERILE. Hence, when hand over the face shields to the end users, we have made sure to remind them to sanitize the face shields before use. Seeing that they are medical practitioners, they already know what to do.</p>
                        </div><!-- /.toggle-content -->
                    </div><!-- /.flat-toggle -->

                    <div class="flat-toggle">
                        <h6 class="toggle-title">6. Who can we contact if we want to know more about this initiative?</h6>
                        <div class="toggle-content">
                            <p>We’ll be happy to respond to you. We apologize in advance for any delay in responses.</p>

                            <p>For questions about logistics and communication : <strong>Adrian Wong <a href="mailto:adrian@cornerroomstudios.com" target="_blank">adrian@cornerroomstudios.com</a></strong></p>

                            <p>Follow our progress at <a href="https://www.facebook.com/groups/3195817283976982" target="_blank"><strong>https://www.facebook.com/groups/3195817283976982</strong></a></p>
                        </div><!-- /.toggle-content -->
                    </div><!-- /.flat-toggle -->

                </div><!-- /.flat-accordion -->
            </div><!-- /.general -->

            @include('includes.sidebar')

        </div><!-- /.row -->
    </div><!-- /.container -->
</div><!-- /.blog -->
@endsection