@extends('layouts.index')

@section('content')
<div class="page-breadcrumbs">
    <div class="container">
        <div class="row">
            <div class="flat-wrapper">
                <div class="breadcrumbs">
                    <h2 class="trail-browse">You are here:</h2>
                    <ul class="trail-items">
                        <li class="trail-item"><a href="{{ route('home') }}">Homepage</a></li>
                        <li>Order/Request</li>
                    </ul>
                </div><!-- /.breadcrumbs -->
            </div><!-- /.flat-wrapper -->
        </div><!-- /.row -->
    </div><!-- /.container -->
</div><!-- /.page-breadcrumbs -->

<div class="flat-row flat-general sidebar-left pad-bottom80px">
    <div class="container">
        <div class="row">
            <div class="general">

                <h3 class="flat-title-section style"><span>Order</span> our Face Shield</h3>

                <div class="widget widget_text">
                    <div class="textwidget">
                        <div class="content-text">
                            <div style="">
                                <p>At the moment we need you to fill up our Google Form to order our Face Shield.
                                Please join our <a href="" target="_blank" style="color: #FFF"><u>Facebook Group</u></a> for more information.<br><br>
                                </p>
                            </div>
                            <a class="button white" style="margin-bottom: 5px;" target="_blank" href="https://docs.google.com/forms/d/e/1FAIpQLSfZtHS12mrVuPUBoML8sa4zXC81yl2gVSa6G0l_TgBVPrJj7A/viewform">Google Form<i class="fa fa-chevron-right"></i></a>
                            <a class="button white" style="margin-bottom: 5px;" target="_blank" href="https://www.facebook.com/groups/teamacovid19">Facebook Group<i class="fa fa-chevron-right"></i></a>
                        </div>
                    </div><!-- /.textwidget -->
                </div><!-- /.widget_text -->

                <div class="flat-divider d30px"></div>



            </div><!-- /.general -->

            @include('includes.sidebar')

        </div><!-- /.row -->
    </div><!-- /.container -->
</div><!-- /.blog -->
@endsection