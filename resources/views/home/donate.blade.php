@extends('layouts.index')

@section('content')
<div class="page-breadcrumbs">
    <div class="container">
        <div class="row">
            <div class="flat-wrapper">
                <div class="breadcrumbs">
                    <h2 class="trail-browse">You are here:</h2>
                    <ul class="trail-items">
                        <li class="trail-item"><a href="{{ route('home') }}">Homepage</a></li>
                        <li>Make Donation</li>
                    </ul>
                </div><!-- /.breadcrumbs -->
            </div><!-- /.flat-wrapper -->
        </div><!-- /.row -->
    </div><!-- /.container -->
</div><!-- /.page-breadcrumbs -->

<div class="flat-row flat-general sidebar-left pad-bottom80px">
    <div class="container">
        <div class="row">
            <div class="general">

                <h3 class="flat-title-section style"><span>Donation</span> can be made to</h3>

                <div class="widget widget_text">
                    <div class="textwidget">
                        <div class="content-text">
                            <div style="">
                                <p>
                                    Company Full Name : YOUTH TRUST FOUNDATION (912397-P) <br>
                                    Maybank Number : 512763114499 <br>
                                    Email: admin@myharapan.org <br>
                                    Please reference : Frontliners or C19
                                </p>
                            </div>
                        </div>
                    </div><!-- /.textwidget -->
                </div><!-- /.widget_text -->

                @if(1==2)
                <h3 class="flat-title-section style">We also accept <span>Boost E-Wallet</span></h3>
                @endif

                <img src="{{ asset('images/poster.png') }}">

                <br>
                <p style="font-weight: bold; font-size: 15px;">No amount is too small. 100% of contributions will go
                    towards materials & logistics to deliver them to KKM Hospitals in need.</p>


                <div class="flat-divider d30px"></div>

            </div><!-- /.general -->

            @include('includes.sidebar')

        </div><!-- /.row -->
    </div><!-- /.container -->
</div><!-- /.blog -->
@endsection