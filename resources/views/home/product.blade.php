@extends('layouts.index')

@section('content')
<div class="page-breadcrumbs">
    <div class="container">
        <div class="row">
            <div class="flat-wrapper">
                <div class="breadcrumbs">
                    <h2 class="trail-browse">You are here:</h2>
                    <ul class="trail-items">
                        <li class="trail-item"><a href="{{ route('home') }}">Homepage</a></li>
                        <li>Product</li>
                    </ul>
                </div><!-- /.breadcrumbs -->
            </div><!-- /.flat-wrapper -->
        </div><!-- /.row -->
    </div><!-- /.container -->
</div><!-- /.page-breadcrumbs -->

<div class="flat-row flat-general sidebar-left pad-bottom80px" style="padding-top: 30px;">
    <div class="container">
        <div class="row" style="padding-left: 15px;">
            
        <h3 style="margin-top: 0px;" class="flat-title-section style">#BreakTheChain <span>{{ $product->name }}</span></h3>

        @if($product->id === '1')
        <div class="row" style="margin-bottom: 10px;">
            <div class="col-md-12">
                <img src="images/products/faceshield2.jpg" alt="images">
            </div>
        </div>

        <div class="general-text">
            <p>TeaMa Face Shield was a name given by an American on 3D Printing COVID-19 Server on Discord. Thus far, we have 143 owners of 3D printers nationwide that are involved with this volunteerism MOVEMENT, higher education colleges and universities like UC TATI, MMU just to name a few and private companies who have dedicated their MCO and work-from-home time to monitor their 3D printers. Every state MOVEMENT is in touch with their respective Covid-19 screening Hospitals. They have their own CHAPTER where it consist of fulfilling supply and demand with a state lead, state team of printers, runners and Health Care Workers (HCW).</p>
            <p>To date, we have printed more than 20,000 Face Shields (version 2 revision 5 that was designed based on the PURSA Face Shield) with a slight modification to have zero moving parts and no rubber band. It takes 40 minutes to print one unit but its reusable as it can be easier sanitized.</p>
        
            <h3 class="flat-title-section style">File and <span>Resources</span></h3>
            <div class="widget widget_text">
                <div class="textwidget">
                    <div class="content-text">
                        <a class="button white" style="margin-bottom: 5px;" target="_blank" href="https://drive.google.com/file/d/1ByHE0J7E5ByYEql9UHCqKd1xVhxIa7y0/view">Appreciative Letter<i class="fa fa-chevron-right"></i></a>
                    </div>
                </div>
            </div>

            <iframe width="560" height="315" src="https://www.youtube.com/embed/E95k08UwPxE" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
        </div>
        
        @elseif($product->id === '2')
        <div class="row" style="margin-bottom: 10px;">
            <div class="col-md-6">
                <img src="images/products/bbox1.png" alt="images">
            </div>
            <div class="col-md-6">
                <img src="images/products/bbox2.png" alt="images">
            </div>
        </div>

        <div class="general-text">
            <p>Version v2.5+ of our Codename: <strong>Bubble Box</strong> for <strong>Covid-19</strong> patients. <a href="https://www.facebook.com/groups/teamacovid19">TeaMa</a> is working alongside <strong>Dr Ammar</strong> from <strong>Hospital Sg Buloh</strong> on the <strong>design and functionalities</strong> of the box. There has been requests from Dr Zulhilmi (Kota Bahru), Thompson Hosp. and Ipoh GH (Dr. Lee & Elly Leong).</p>

            <p><strong><u>PLEASE DO NOT UNNECESSARY MANUFACTURE BOXES THAT ARE FROM TAIWAN AND CHINA AS THEY ARE NOT BASED ON HOSPITAL SG BULOH'S TRIALS & TESTING</u></strong></p>
            
            <p>It comes with filtered <strong>bagging and negative pressure suctioning</strong>. We are working on Ver3 and to have a <strong>portable magnetic pump that is battery-operated.</strong></p>

            <p>There are 3 parts of the box:</p>

            <ul>
                <li>Box made from acrylic, hinge and lock.</li>
                <li>3D printed 4x units of connectors (2 sets needed for Emergency Dept.</li>
                <li>Suction pump that is battery operated for portability.</li>
            </ul>

            <p><strong>Cosine helps your sales force become more effective by providing a continuous learning environment.</strong></p>
            <p>Cosine&nbsp;is a global&nbsp;sales training&nbsp;company that offers sales effectiveness consulting and assessments. Our approach is highly collaborative, with a focus on enabling the right sales activity and effective customer dialogues.&nbsp;Through our&nbsp;<strong>Sales Effectiveness System</strong><strong>™</strong>,&nbsp;we help you improve sales results. We do this in three ways to create a continuous learning and performance development loop:</p>
            <ul class="flat-list style1">
                <li>We <strong>diagnose and assess</strong> the talent and structure of your sales force</li>
                <li>We create customized solutions that leverages our building block sales training curriculum</li>
                <li>We <strong>reinforce learning</strong> to drive behavior change through coaching and digital learning sustainment tools</li>
            </ul>

            <h3 class="flat-title-section style"><span>Ventilator Box</span> Instructions</h3>
            <h4>V3 With Magnet and a Hinge</h4>
            <p style="text-align: center;">
                <img src="images/products/bbox3.jpg" alt="images">
            </p>
            <p><b><u>V3 IS NOT finalised</u></b> until simulation test are done by Dr Ammar and his esteemed teammates.</p>
            <p>We are in the midst of working on the <b>Reverse Pressure Suctioning (RPS)</b> - which I call it a <i>Mamak Hood</i>.</p>
            <p>The <i>Mamak Hood</i> is meant to suck the air inside the Bubble Box like how your kitchen's hood sucks out your sambal petai smells :)</p>
            
            <h4>Precision and Mistakes We Made of 0.25mm!</h4>
            <div class="row" style="margin-bottom: 10px;">
                <div class="col-md-8">
                    <img src="images/products/bbox4.jpg" alt="images">
                </div>
                <div class="col-md-4">
                    <img src="images/products/bbox5.jpg" alt="images">
                    <img src="images/products/bbox6.jpg" alt="images">
                </div>
            </div>
            <p>Please start calibrating your 3D printers to print these.</p>
            <p>Our first badge by Azman was 0.25mm short and it won't fit snuggly.</p>

            <h4>V2.5 Reiterate, Test, Prototype, Repeat</h4>
            <div class="row" style="margin-bottom: 10px;">
                <div class="col-md-6">
                    <img src="images/products/bbox7.jpg" alt="images">
                </div>
                <div class="col-md-6">
                    <img src="images/products/bbox8.jpg" alt="images">
                </div>
            </div>
            <p>Changes from V2</p>
            <ul>
                <li>1. 2 additional hinges</li>
                <li>2. Relocations of the 3.5cm Ventilation Tube</li>
                <li>3. Hold for holds enlarged (1.2cm diameter)</li>
                <li>4. Acrylic layer to cover gaps by bed</li>
            </ul>

            <h4>Hose Connected to 4x Adapters</h4>
            <div class="row" style="margin-bottom: 10px;">
                <div class="col-md-6">
                    <img src="images/products/bbox9.jpg" alt="images">
                </div>
                <div class="col-md-6">
                    <img src="images/products/bbox10.jpg" alt="images">
                </div>
            </div>
        </div>
        
        @elseif($product->id === '3')
        <div class="row" style="margin-bottom: 10px;">
            <div class="col-md-6">
                <img src="images/products/dmask1.png" alt="images">
            </div>
            <div class="col-md-6">
                <img src="images/products/dmask2.png" alt="images">
            </div>
        </div>

        <div class="general-text">
            <p><strong><a href="https://www.isinnova.it/easy-covid19-eng/">Charlotte Valve by the Italians</a> are brilliant!</strong></p>
            <p>We have 3d printed some at TeaMa with multiple heads for multiple users. Here's a breakdown:</p>
            <ul class="flat-list style1">
                <li><strong>Patient</strong>  - objective is to provide ventilation and prevent the spread of Covid-19 positive patients. Good thing is that the Decathlon EasyBreath Snorkelling Mask comes in 3 sizes : Junior, Women and Men. We are doing trials in several hospitals (Covid-17 screening hospitals) across Malaysia.</li>
                <li>Healthcare Worker (HCW) - <strong>Supplied Air Respirator</strong>,  though with a limited field of view, it sure beats conventional masks that start at USD$100-2,000!</li>
                <li>HCW - <strong>Powered Air Purifying Respirator (PAPR)</strong> for Ambulance Services. This has always been the direction we are heading with the Reverse Pressure Pumping!</li>
            </ul>
            
            <h3 class="flat-title-section style"><span>Dmask</span> Connectors</h3>
            <h4>Additional Connectors & Universal Adapter for Malaysia Using the DMask</h4>
            <div class="row" style="margin-bottom: 10px;">
                <div class="col-md-8">
                    <img src="images/products/dmask3.jpg" alt="images">
                </div>
                <div class="col-md-4">
                    <img src="images/products/dmask4.jpg" alt="images">
                    <img src="images/products/dmask5.jpg" alt="images">
                </div>
            </div>
            <p>These are additional connectors for the DMask, that interfaces with the Charlotte Valve. The Charlotte Valve has undergone a thickness enhancement, after rigorous our testing.</p>
            <p>This multi-purpose F-adapter is well suited for Malaysian Hospitals. Its flexibility creates a dual purpose function; where the inner diameter is used for one thing (i.e., Nebuliser) and the outer diameter, can be used to connect to existing Hospital tubings.</p>
            
            <h4>Colour Coded Adapters to the Charlotte Valve</h4>
            <div class="row" style="margin-bottom: 10px;">
                <div class="col-md-6">
                    <img src="images/products/dmask6.jpg" alt="images">
                </div>
            </div>
            <p>GRAY = Main - Charlotte Valve - v1r2 - Increase wall thickness</p>
            <p>RED = Outgoing Center Adapter from Charlotte Valve to Air Filter - V1R2</p>
            <p>GOLD = Incoming Big O2 F-Adapter to Charlotte Valve - V3R1 (V3R2 - with tree support)</p>
            <p>GREEN = Incoming Small Tube Adapter to Big O2 F-Connector - V3R1</p>
            
            <h3 class="flat-title-section style">File and <span>Resources</span></h3>
            <div class="widget widget_text">
                <div class="textwidget">
                    <div class="content-text">
                        <a class="button white" style="margin-bottom: 5px;" target="_blank" href="https://www.facebook.com/groups/teamacovid19/files">Download Files<i class="fa fa-chevron-right"></i></a>
                    </div>
                </div>
            </div>
        </div>

        @endif

        </div><!-- /.row -->
    </div><!-- /.container -->
</div><!-- /.blog -->
@endsection