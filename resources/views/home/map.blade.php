@extends('layouts.index')

@section('head')
<link rel="stylesheet" href="https://unpkg.com/leaflet@1.6.0/dist/leaflet.css"
    integrity="sha512-xwE/Az9zrjBIphAcBb3F6JVqxf46+CDLwfLMHloNu6KEQCAWi6HcDUbeOfBIptF7tcCzusKFjFw2yuvEpDL9wQ=="
    crossorigin="" />
<!-- Make sure you put this AFTER Leaflet's CSS -->
<script src="https://unpkg.com/leaflet@1.6.0/dist/leaflet.js"
    integrity="sha512-gZwIG9x3wUXg2hdXF6+rVkLF/0Vi9U8D2Ntg4Ga5I5BZpVkVxlJWbSQtXPSiUTtC0TjtGOmxa1AJPuV0CPthew=="
    crossorigin=""></script>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/leaflet.locatecontrol@0.71.0/dist/L.Control.Locate.min.css" />

<script src="https://cdn.jsdelivr.net/npm/leaflet.locatecontrol@0.71.0/dist/L.Control.Locate.min.js" charset="utf-8">
</script>
@endsection

@section('content')
<div class="flat-row flat-general pad-bottom80px">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div id="mapid" style="width: 1200px; height: 800px;"></div>
                <script>
                    var mymap = L.map('mapid').setView([3.128239, 101.73924811], 7);
                    var greenIcon = L.icon({
                    iconUrl: '/icon/hospital.jpg',
                    // shadowUrl: 'leaf-shadow.png',
                    
                    iconSize: [28, 20], // size of the icon
                    shadowSize: [50, 64], // size of the shadow
                    iconAnchor: [22, 94], // point of the icon which will correspond to marker's location
                    shadowAnchor: [4, 62], // the same for the shadow
                    popupAnchor: [-3, -76] // point from which the popup should open relative to the iconAnchor
                    });
                
                	L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw', {
                		maxZoom: 18,
                		attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, ' +
                			'<a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, ' +
                			'Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
                		id: 'mapbox/streets-v11',
                		tileSize: 512,
                		zoomOffset: -1
                    }).addTo(mymap);

                    L.control.locate().addTo(mymap);
                    
                    @foreach($volunteers as $volunteer)
                    L.marker([{{$volunteer->gmap_lat}},{{$volunteer->gmap_long}}]).addTo(mymap)
                    .bindPopup("<b>Total Printer : </b>{{$volunteer->printer_all}}").openPopup();
                   @endforeach

                   @foreach($hospitals as $hospital)
                    L.marker([{{$hospital->latitude}},{{$hospital->longitude}}], {icon: greenIcon}).addTo(mymap)
                    .bindPopup("{{$hospital->name}}").openPopup();
                    @endforeach
                    
                    var popup = L.popup();
                
                </script>
            </div>
        </div>
    </div>
</div>
@endsection