@extends('layouts.index')

@section('content')

<div class="flat-row flat-general pad-bottom80px">
    <div class="container">
        <div class="row">
            <div class="col-md-12">

                <h3 class="flat-title-section style">What do you <span>want to do?</span></h3>

                <div class="row">
                    <div class="col-md-4">
                        <div class="widget widget_text" style="margin-bottom: 20px;">
                            <div class="textwidget">
                                <div class="content-text">
                                    <h4 class="title">Order Face Shileds</h4>
                                    <div style="min-height: 140px">
                                        <p>If you are a frontliner, or your hospital needs Face Shields, please click the button below to request Face Shields from us.</p>
                                    </div>
                                    <a class="button white" href="{{ route('request') }}">Order<i class="fa fa-chevron-right"></i></a>
                                </div>
                            </div><!-- /.textwidget -->
                        </div><!-- /.widget_text -->
                    </div>
                    <div class="col-md-4">
                        <div class="widget widget_text" style="margin-bottom: 20px;">
                            <div class="textwidget">
                                <div class="content-text">
                                    <h4 class="title">Become a Volunteer</h4>
                                    <div style="min-height: 140px">
                                        <p>Do you have a 3D printer at your house or workplace? Please join us and contribute.</p>
                                    </div>
                                    <a class="button white" href="{{ route('volunteer') }}">Volunteer<i class="fa fa-chevron-right"></i></a>
                                </div>
                            </div><!-- /.textwidget -->
                        </div><!-- /.widget_text -->
                    </div>
                    <div class="col-md-4">
                        <div class="widget widget_text" style="margin-bottom: 20px;">
                            <div class="textwidget">
                                <div class="content-text">
                                    <h4 class="title">I want to Donate</h4>
                                    <div style="min-height: 140px">
                                        <p>No amount is too small. 100% of contributions will go towards materials & logistics to deliver them to KKM Hospitals in need.</p>
                                    </div>
                                    <a class="button white" href="{{ route('donate') }}">Make Donation<i class="fa fa-chevron-right"></i></a>
                                </div>
                            </div><!-- /.textwidget -->
                        </div><!-- /.widget_text -->
                    </div>
                </div>

                <div class="flat-divider d30px"></div>

                <h3 class="flat-title-section style">Progress <span>Status</span></h3>

                <div class="row">
                    <div class="col-md-4 col-xs-12" style="margin-top: 20px;">
                        <div style="background-color: #9beaf2; padding: 20px;">
                            <div style="font-size: 20px; color: #444;">Donation Collected</div>
                            <div class="numb-count" style="font-size: 40px; color: #00851b; font-weight: bold; line-height: 50px">RM {{ number_format($data->donation, 2, '.', ',') }}</div>
                        </div>
                    </div>
                    <div class="col-md-4 col-xs-12" style="margin-top: 20px;">
                        <div style="background-color: #f2db9b; padding: 20px;">
                            <div style="font-size: 20px; color: #444;">Estimated Cost Needed</div>
                            <div class="numb-count" style="font-size: 40px; color: #d90000; font-weight: bold; line-height: 50px">RM {{ number_format($data->demand * 3, 2, '.', ',') }}</div>
                        </div>
                    </div>
                    <div class="col-md-4 col-xs-12" style="margin-top: 20px;">
                        <div style="background-color: #EEE; padding: 20px;" class="counter">
                            <div style="font-size: 20px; color: #444;">Total Orders Received</div>
                            <div class="numb-count" style="font-size: 45px; color: #000; font-weight: bold; line-height: 50px" data-to="{{ $data->demand }}" data-speed="2000" data-waypoint-active="yes">{{ $data->demand }}</div>
                        </div>
                    </div>
                </div>

                <div class="flat-divider d30px"></div>

                <h3 class="flat-title-section style">Blood <span>Donation</span></h3>

                <div class="row">
                    <div class="col-md-12 col-xs-12" style="margin-top: 20px;">
                        <div style="background-color: #a83232; padding: 20px;">
                            <div class="row">
                                <div class="col-md-8">
                                    <div style="font-size: 30px; color: #FFF; font-weight: bold; line-height: 30px; padding-top:8px; padding-bottom: 5px;">Come to my house so that I can donate blood!</div>
                                </div>
                                <div class="col-md-4" style="padding-top: 5px;">
                                    <a href="{{ route('blood') }}" class="btn btn-xl btn-warning pull-right" style="font-weight: bold; font-size: 20px; color: #000; background-color: #f7d40f">Blood Donation Form &nbsp; <i class="fa fa-chevron-right"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="flat-divider d30px"></div>              

            </div><!-- /.general -->

        </div><!-- /.row -->
    </div><!-- /.container -->
</div><!-- /.blog -->
@endsection