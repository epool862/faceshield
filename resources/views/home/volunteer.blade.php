@extends('layouts.index')

@section('content')
<div class="page-breadcrumbs">
    <div class="container">
        <div class="row">
            <div class="flat-wrapper">
                <div class="breadcrumbs">
                    <h2 class="trail-browse">You are here:</h2>
                    <ul class="trail-items">
                        <li class="trail-item"><a href="{{ route('home') }}">Homepage</a></li>
                        <li>Volunteer</li>
                    </ul>
                </div><!-- /.breadcrumbs -->
            </div><!-- /.flat-wrapper -->
        </div><!-- /.row -->
    </div><!-- /.container -->
</div><!-- /.page-breadcrumbs -->

<div class="flat-row flat-general sidebar-left pad-bottom80px">
    <div class="container">
        <div class="row">
            <div class="general">

                <h3 class="flat-title-section style">Becoming <span>VOLUNTEER</span></h3>

                <div class="widget widget_text">
                    <div class="textwidget">
                        <div class="content-text">
                            <div style="">
                                <p>At the moment we need you to fill up our form to become a volunteer. 
                                Please join our <a href="" target="_blank" style="color: #FFF"><u>Facebook Group</u></a> for more information.<br><br>
                                </p>
                            </div>
                            @if(1==2)
                            <a class="button white" style="margin-bottom: 5px;" target="_blank" href="https://docs.google.com/forms/d/e/1FAIpQLSf5P7NrQWwYb06et71QGnqUw4d1n7vw8qNVw960LTuXWMWuOA/viewform">Google Form<i class="fa fa-chevron-right"></i></a>
                            @endif
                            <a class="button white" style="margin-bottom: 5px;" target="_blank" href="{{ route('volunteer2') }}">Registration Form<i class="fa fa-chevron-right"></i></a>
                            <a class="button white" style="margin-bottom: 5px;" target="_blank" href="https://www.facebook.com/groups/teamacovid19">Facebook Group<i class="fa fa-chevron-right"></i></a>
                        </div>
                    </div><!-- /.textwidget -->
                </div><!-- /.widget_text -->

                <h3 class="flat-title-section style">File and <span>RESOURCES</span></h3>

                <div class="widget widget_text">
                    <div class="textwidget">
                        <div class="content-text">
                            <h2 class="title">Free Download STL Files</h2>
                            <ul>
                                <li><i class="fa fa-arrow-circle-right"></i> <a href="https://drive.google.com/drive/folders/1s1yQXU2ufyrxTMoJ01mafViderytpuar?fbclid=IwAR2xnGbK8msiKZORByoylRqwW1GnKOm7tDiPJHCeNmZU2jUo7my65vtyY2Y" target="_blank" style="color: #FFF;">Ver1 (Legacy)</a></li>
                                <li><i class="fa fa-arrow-circle-right"></i> <a href="https://drive.google.com/drive/folders/1s1yQXU2ufyrxTMoJ01mafViderytpuar?fbclid=IwAR2xnGbK8msiKZORByoylRqwW1GnKOm7tDiPJHCeNmZU2jUo7my65vtyY2Y" target="_blank" style="color: #FFF;">Ver2 (Legacy)</a></li>
                                <li><i class="fa fa-arrow-circle-right"></i> <a href="https://drive.google.com/drive/folders/1s1yQXU2ufyrxTMoJ01mafViderytpuar?fbclid=IwAR2xnGbK8msiKZORByoylRqwW1GnKOm7tDiPJHCeNmZU2jUo7my65vtyY2Y" target="_blank" style="color: #FFF;">Ver2 R2 (Legacy)</a></li>
                                <li><i class="fa fa-arrow-circle-right"></i> <a href="https://drive.google.com/drive/folders/1s1yQXU2ufyrxTMoJ01mafViderytpuar?fbclid=IwAR2xnGbK8msiKZORByoylRqwW1GnKOm7tDiPJHCeNmZU2jUo7my65vtyY2Y" target="_blank" style="color: #FFF;">Ver2 R3</a></li>
                                <li><i class="fa fa-arrow-circle-right"></i> <a href="https://drive.google.com/drive/folders/1s1yQXU2ufyrxTMoJ01mafViderytpuar?fbclid=IwAR2xnGbK8msiKZORByoylRqwW1GnKOm7tDiPJHCeNmZU2jUo7my65vtyY2Y" target="_blank" style="color: #FFF;">Ver2 R5</a></li>
                            </ul>
                            <hr>
                            <h2 style="color: #FFF">Need Filament?</h2>
                            <a class="button white" style="margin-bottom: 5px;" target="_blank" href="https://docs.google.com/forms/d/136lHR178GdjrbtGag8YUEzb1uZSRPqhnOVop0BuXBr8/viewform?edit_requested=true">Filament Request Form<i class="fa fa-chevron-right"></i></a>
                            <br>
                            <h2 style="color: #FFF">Need A4 Transparent Sheet?</h2>
                            <a class="button white" style="margin-bottom: 5px;" target="_blank" href="https://forms.gle/XhbRbhNWVh4dBgd68">A4 Transparent Request Form<i class="fa fa-chevron-right"></i></a>
                        </div>
                    </div><!-- /.textwidget -->
                </div><!-- /.widget_text -->

                <h3 class="flat-title-section style">Video <span>TUTORIAL</span></h3>

                <div class="row">
                    <div class="col-md-6">
                        <iframe width="100%" height="200" src="https://www.youtube.com/embed/TJszpZpcWNU" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                    </div>
                    <div class="col-md-6">
                        <iframe width="100%" height="200" src="https://www.youtube.com/embed/lbq7MMt6ZHI" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                    </div>
                </div>

                <div class="flat-divider d30px"></div>



            </div><!-- /.general -->

            @include('includes.sidebar')

        </div><!-- /.row -->
    </div><!-- /.container -->
</div><!-- /.blog -->
@endsection