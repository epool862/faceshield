<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

Auth::routes();

Route::get('/', 'HomeController@index')->name('home');
Route::get('/request', 'HomeController@request')->name('request');
Route::get('/product', 'HomeController@product')->name('product');
Route::get('/volunteer', 'HomeController@volunteer')->name('volunteer');
Route::get('/donate', 'HomeController@donate')->name('donate');
Route::get('/faq', 'HomeController@faq')->name('faq');
Route::get('/sheet', 'HomeController@sheet')->name('sheet');
Route::get('/map', 'HomeController@map')->name('map');

Route::get('/blood', 'BloodController@index')->name('blood');
Route::get('/unverify', 'BloodController@unverify')->name('blood.unverify');
Route::get('/bloodpost', 'BloodController@bloodget')->name('bloodget');
Route::post('/bloodpost', 'BloodController@bloodpost')->name('bloodpost');

Route::get('/printer', 'VolunteerController@index')->name('volunteer2');
Route::get('/printerpost', 'VolunteerController@volunteerget')->name('volunteerget');
Route::post('/printerpost', 'VolunteerController@volunteerpost')->name('volunteerpost');
//Route::get('/printerdb', 'VolunteerController@postdb')->name('postdb');

Route::group(['middleware' => ['auth', 'adminVerify']], function () {

    Route::get('/admin', 'AdminController@index')->name('admin');
    Route::get('/admin/dashboard', 'AdminController@dashboard')->name('admin.dashboard');
    Route::get('/admin/list', 'AdminController@list')->name('admin.list');
    Route::get('/admin/export', 'AdminController@export')->name('admin.export');

});

Route::get('/admin/unverify', 'AdminController@unverify')->name('admin.unverify');