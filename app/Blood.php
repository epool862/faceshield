<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Blood extends Model
{

	use SoftDeletes;

    protected $fillable = [
        "name","ic","blood_type","blood_group","blood_test","unit","address_1","address_2","postcode","city","state","country","phone","mobile","email",
        "age","gender","race","religion","gmap_lat","gmap_long","gmap_addr","gmap_level","remark","status","type","group",
    ];

}
