<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Data extends Model
{
	protected $table = 'datas';

    protected $fillable = [
        "date","hour","donation","printer","demand","delivered","balance","printed","onhand",
        "online","spool","a4",
    ];
}
