<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Volunteer extends Model
{
    use SoftDeletes;

    protected $fillable = [
        "name","fb","address_1","address_2","postcode","city","state","country","phone","mobile","email",
        "printer_model","printer_all","printer_online","perday","total_production","total_filament",
        "gmap_lat","gmap_long","gmap_addr","gmap_level","remark","status","type","group",
    ];
}
