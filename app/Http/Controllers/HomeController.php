<?php

namespace App\Http\Controllers;

use App\Data;
use App\Hospital;
use App\Volunteer;
use Illuminate\Http\Request;
use Revolution\Google\Sheets\Sheets;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $data = Data::latest()->first();
        return view('home.index', compact('data'));
    }

    public function request()
    {
        return view('home.request');
    }

    public function product(Request $request)
    {
        $product_id = array(1, 2, 3);
        $product_name = array('Face Shield', 'Intubation Box', 'Dmask');

        $product = new \StdClass;
        

        if(in_array($request->id, $product_id)){
            $product->name = $product_name[(int)$request->id - 1];
            $product->id = $request->id;

            return view('home.product', compact('product'));
        }
        else{
            $product->name = $product_name[0];
            $product->id = '1';

            return view('home.product', compact('product'));
        }
    }

    public function volunteer()
    {
        return view('home.volunteer');
    }

    public function map()
    {
        $volunteers = Volunteer::all();
        $hospitals = Hospital::all();

        return view('home.map', compact('volunteers', 'hospitals'));
    }

    public function donate()
    {
        return view('home.donate');
    }

    public function faq()
    {
        return view('home.faq');
    }

    public function sheet()
    {

        $rows = Data::latest()->first();

        return json_encode([
            "donation" => $rows->donation,
            "printer" => $rows->printer,
            "demand" => $rows->demand,
            "delivered" => $rows->delivered,
            "balance" => $rows->balance,
            "printed" => $rows->printed,
            "onhand" => $rows->onhand,
            "online" => $rows->online,
            "spool" => $rows->spool,
            "a4" => $rows->a4,
            "updated_at" => $rows->updated_at->format("H:i:s d/m/Y"),
        ]);

    }

    public function updateSheet()
    {

        $sheet = new Sheets;

        $rows1 = $sheet->spreadsheet('1VziS-CzKIyLdzXEtzFeVRGEbdCy2Gifag-jhXf_JJSE')->sheet('D2')->range('B7')->all();
        $rows2 = $sheet->spreadsheet('1VziS-CzKIyLdzXEtzFeVRGEbdCy2Gifag-jhXf_JJSE')->sheet('D2')->range('G7')->all();
        $rows3 = $sheet->spreadsheet('1VziS-CzKIyLdzXEtzFeVRGEbdCy2Gifag-jhXf_JJSE')->sheet('D2')->range('B11')->all();
        $rows4 = $sheet->spreadsheet('1VziS-CzKIyLdzXEtzFeVRGEbdCy2Gifag-jhXf_JJSE')->sheet('D2')->range('C11')->all();
        $rows5 = $sheet->spreadsheet('1VziS-CzKIyLdzXEtzFeVRGEbdCy2Gifag-jhXf_JJSE')->sheet('D2')->range('D11')->all();
        $rows6 = $sheet->spreadsheet('1VziS-CzKIyLdzXEtzFeVRGEbdCy2Gifag-jhXf_JJSE')->sheet('D2')->range('E11')->all();
        $rows7 = $sheet->spreadsheet('1VziS-CzKIyLdzXEtzFeVRGEbdCy2Gifag-jhXf_JJSE')->sheet('D2')->range('F11')->all();
        $rows8 = $sheet->spreadsheet('1VziS-CzKIyLdzXEtzFeVRGEbdCy2Gifag-jhXf_JJSE')->sheet('D2')->range('G11')->all();
        $rows9 = $sheet->spreadsheet('1VziS-CzKIyLdzXEtzFeVRGEbdCy2Gifag-jhXf_JJSE')->sheet('D2')->range('E7')->all();
        $rows10 = $sheet->spreadsheet('1VziS-CzKIyLdzXEtzFeVRGEbdCy2Gifag-jhXf_JJSE')->sheet('D2')->range('F7')->all();

        $exchange = Data::updateOrCreate([
            "date" => date("Y-m-d"),
            "hour" => date("H"),
        ], [
            "donation" => preg_replace("/[^0-9]/", "", $rows1[0][0]),
            "printer" => $rows2[0][0],
            "demand" => $rows3[0][0],
            "delivered" => $rows4[0][0],
            "balance" => $rows5[0][0],
            "printed" => $rows6[0][0],
            "onhand" => $rows7[0][0],
            "online" => $rows8[0][0],
            "spool" => $rows9[0][0],
            "a4" => $rows10[0][0],
        ]);

    }
}