<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Volunteer;
use PulkitJalan\Google\Facades\Google;
use Revolution\Google\Sheets\Sheets;

class VolunteerController extends Controller
{
    
    public function index(){

        return view('volunteer.form');

    }

    public function volunteerpost(Request $request){

        $this->validate($request, [
            "name" => "required|min:2|max:160",
            "mobile" => "required|min:10|max:30",
            "printer_all" => "nullable|digits_between:0,999",
            "perday" => "nullable|digits_between:0,9999",
            "address_1" => "nullable|max:250",
            "postcode" => "nullable|max:250",
            "city" => "nullable|max:250",
            "state" => "required",
            "gmap_lat" => "required",
            "gmap_long" => "required",
        ],[],[
            "name" => "Name",
        ]);

        $volunteer = new Volunteer;

        $volunteer->fill($request->except([]));

        $volunteer->printer_all = $request['printer_all'] ? $request['printer_all'] : 0;
        $volunteer->perday = $request['perday'] ? $request['perday'] : 0;

        $volunteer->save();

        $doc_id = '1VziS-CzKIyLdzXEtzFeVRGEbdCy2Gifag-jhXf_JJSE';
        $sheet_id = 'Location';

        $append = [
            $volunteer->id,
            $volunteer->name ? $volunteer->name : '',
            $volunteer->mobile ? $volunteer->mobile : '',
            $volunteer->printer_model ? $volunteer->printer_model : '',
            $volunteer->printer_all ? $volunteer->printer_all * 1 : 0,
            $volunteer->perday ? $volunteer->perday * 1 : 0,
            $volunteer->address_1 ? $volunteer->address_1 : '',
            $volunteer->postcode ? $volunteer->postcode : '',
            $volunteer->city ? $volunteer->city : '',
            $volunteer->state ? $volunteer->state : '',
            $volunteer->gmap_lat ? $volunteer->gmap_lat : '',
            $volunteer->gmap_long ? $volunteer->gmap_long : '',
            $volunteer->created_at->format('d/m/Y H:i:s'),
        ];

        $sheet = new Sheets;

        $sheet->spreadsheet($doc_id)
              ->sheet($sheet_id)
              ->append([$append]);
        

        return view('volunteer.thanks');

    }

    public function volunteerget(){

        return view('volunteer.thanks');
        
    }

    public function postDB(){

        $volunteers = Volunteer::all();
        $doc_id = '1VziS-CzKIyLdzXEtzFeVRGEbdCy2Gifag-jhXf_JJSE';
        $sheet_id = 'Location';

        foreach ($volunteers as $volunteer) {
            
            $append = [
                $volunteer->id,
                $volunteer->name ? $volunteer->name : '',
                $volunteer->mobile ? $volunteer->mobile : '',
                $volunteer->printer_model ? $volunteer->printer_model : '',
                $volunteer->printer_all ? $volunteer->printer_all : 0,
                $volunteer->perday ? $volunteer->perday : 0,
                $volunteer->address_1 ? $volunteer->address_1 : '',
                $volunteer->postcode ? $volunteer->postcode : '',
                $volunteer->city ? $volunteer->city : '',
                $volunteer->state ? $volunteer->state : '',
                $volunteer->gmap_lat ? $volunteer->gmap_lat : '',
                $volunteer->gmap_long ? $volunteer->gmap_long : '',
                $volunteer->created_at->format('d/m/Y H:i:s'),
            ];

            $sheet = new Sheets;

            $sheet->spreadsheet($doc_id)
                  ->sheet($sheet_id)
                  ->append([$append]);

        }

    }

}
