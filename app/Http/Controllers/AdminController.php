<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Blood;
use App\Volunteer;

class AdminController extends Controller
{

	public function index(){

		return redirect()->route('admin.dashboard');
	}
    
    public function dashboard(){

    	return view('volunteer.dashboard');

    }

    public function list(){

     	$volunteers = Volunteer::all();

    	return view('volunteer.list', compact('volunteers'));

    }

    public function export(){

        return view('volunteer.export');

    }

    public function unverify(){

    	return view('volunteer.unverify');

    }
    
}
