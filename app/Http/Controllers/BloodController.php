<?php

namespace App\Http\Controllers;

use App\Blood;
use Illuminate\Http\Request;

class BloodController extends Controller
{

    public function index()
    {

        return view('blood.form');

    }

    public function bloodpost(Request $request)
    {

        $this->validate($request, [
            "name" => "required|min:5|max:160",
            "mobile" => "required",
            "address_1" => "required",
            "postcode" => "required",
            "city" => "required",
            "state" => "required",
            "gmap_lat" => "required",
            "gmap_long" => "required",
        ], [], [
            "name" => "Name",
        ]);

        $blood = new Blood;

        $blood->fill($request->except([]));

        $blood->save();

        return view('blood.thanks');

    }

    public function bloodget()
    {

        return view('blood.thanks');

    }

    public function unverify()
    {

        return view('blood.unverify');

    }

}