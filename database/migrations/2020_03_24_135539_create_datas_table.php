<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDatasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('datas', function (Blueprint $table) {

            $table->bigIncrements('id');

            $table->decimal('donation', 15, 4)->default(0);
            $table->decimal('cost', 15, 4)->default(0);
            $table->decimal('printer', 15, 4)->default(0);
            $table->decimal('demand', 15, 4)->default(0);
            $table->decimal('delivered', 15, 4)->default(0);
            $table->decimal('balance', 15, 4)->default(0);
            $table->decimal('printed', 15, 4)->default(0);
            $table->decimal('onhand', 15, 4)->default(0);
            $table->decimal('online', 15, 4)->default(0);
            $table->decimal('spool', 15, 4)->default(0);
            $table->decimal('a4', 15, 4)->default(0);
            $table->date('date')->nullable();
            $table->text('hour')->nullable();

            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('datas');
    }
}
