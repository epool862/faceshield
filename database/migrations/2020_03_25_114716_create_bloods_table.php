<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBloodsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bloods', function (Blueprint $table) {

            $table->bigIncrements('id');

            $table->string('name')->nullable();
            $table->string('ic')->nullable();

            $table->string('blood_type')->nullable();
            $table->string('blood_group')->nullable();
            $table->string('blood_test')->nullable();

            $table->string('unit')->nullable();
            $table->string('address_1')->nullable();
            $table->string('address_2')->nullable();
            $table->string('postcode')->nullable();
            $table->string('city')->nullable();
            $table->string('state')->nullable();
            $table->string('country')->nullable();
            $table->string('phone')->nullable();
            $table->string('mobile')->nullable();
            $table->string('email')->nullable();

            $table->string('age')->nullable();
            $table->string('gender')->nullable();
            $table->string('race')->nullable();
            $table->string('religion')->nullable();

            $table->string('gmap_lat')->nullable();
            $table->string('gmap_long')->nullable();
            $table->string('gmap_addr')->nullable();
            $table->string('gmap_level')->nullable();

            $table->text('remark')->nullable();
            $table->integer('status')->default(0);
            $table->integer('type')->default(0);
            $table->integer('group')->default(0);
            $table->bigInteger('by')->default(0);
            $table->softDeletes();

            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bloods');
    }
}
