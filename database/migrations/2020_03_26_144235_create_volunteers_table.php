<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVolunteersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('volunteers', function (Blueprint $table) {
            
            $table->bigIncrements('id');

            $table->string('name')->nullable();
            $table->string('fb')->nullable();

            $table->string('address_1')->nullable();
            $table->string('address_2')->nullable();
            $table->string('postcode')->nullable();
            $table->string('city')->nullable();
            $table->string('state')->nullable();
            $table->string('country')->nullable();
            $table->string('phone')->nullable();
            $table->string('mobile')->nullable();
            $table->string('email')->nullable();

            $table->string('printer_model')->nullable();
            $table->integer('printer_all')->default(0);
            $table->integer('printer_online')->default(0);
            $table->integer('perday')->default(0);
            $table->integer('total_production')->default(0);
            $table->string('total_filament')->nullable();

            $table->string('gmap_lat')->nullable();
            $table->string('gmap_long')->nullable();
            $table->string('gmap_addr')->nullable();
            $table->string('gmap_level')->nullable();

            $table->text('remark')->nullable();
            $table->integer('status')->default(0);
            $table->integer('type')->default(0);
            $table->integer('group')->default(0);
            $table->bigInteger('by')->default(0);
            $table->softDeletes();

            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('volunteers');
    }
}
